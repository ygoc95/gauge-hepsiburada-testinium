# Hepsiburada İkinci Proje
Kullanıcı hepsiburada anasayfasına gidecek. Login butonuna tıklayacak.Açılan yeni sayfada giriş yapacak.
Giriş yapıldıktan sonra "hesabım" sayfasına tıklayacak.Açılan yeni ekranda isim kısmı doğru mu kontrol edilecek.//
## HepsiBurada kullanici adi kontrolü senaryosu

* Anasayfaya git
* Login butonuna tikla
* "kullanici adi" ve "sifre" bölümünü doldur
* Giriş yap
* Hesabım butonuna tıkla
* "Uye ismi" için ad kontrolü
