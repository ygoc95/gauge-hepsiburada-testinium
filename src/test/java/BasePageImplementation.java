import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;


public class BasePageImplementation {
    static WebDriver chromedriver;
    Actions action;
    String url ="https://www.hepsiburada.com";
    String anasayfaTitle = "Türkiye'nin En Büyük Online Alışveriş Sitesi Hepsiburada.com";
    String girisTitle = "Üye Giriş Sayfası & Üye Ol - Hepsiburada";
    String hesabimTitle = "Hesabım";

    //Her sayfa veya senaryo için kullanılabilecek genel metodlar/stepler


    //ChromeDriver Set Up
    @BeforeScenario
    public void SenaryoOncesi(){
        System.out.println("-----Senaryo başlangıcı----");
        System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
        chromedriver = new ChromeDriver();
        new Actions(chromedriver);
    }


    //ChromeDriver İçin Getter
    public WebDriver getChromedriver() {
        return chromedriver;
    }

    //Anasayfaya gidiş
    @Step("Anasayfaya git")
    public void anasayfayaGit() throws InterruptedException {
        chromedriver.manage().window().maximize();
        chromedriver.get(url);
        Thread.sleep(4000);
        if (chromedriver.getTitle().equals(anasayfaTitle)){
            System.out.println("----ANASAYFA YUKLENDI----");
        }else{
            System.out.println("----ANASAYFA YUKLEME BASARISIZ----");
        }
    }

    //ANASAYFA METODLARI

    // Login butonu tıklama işlemi
    @Step("Login butonuna tikla")
    public void loginButonunaTikla() {

        action.moveToElement(chromedriver.findElement(By.id("myAccount"))).build().perform();
        action.moveToElement(chromedriver.findElement(By.id("login"))).click();

    }

    @Step("Hesabım butonuna tıkla")
    public void hesabimButonunaTikla() {
        if (chromedriver.getTitle().equals(hesabimTitle)) {
            System.out.println("----ANASAYFA YUKLENDI----");
            action.moveToElement(chromedriver.findElement(By.id("myAccount"))).build().perform();
            action.moveToElement(chromedriver.findElement(By.xpath("//*[@id=\"myAccount\"]/div/div/ul/li[5]/a"))).click();
        }else{
            System.out.println("----ANASAYFA YUKLENEMEDI----");
        }
    }


    // LOGIN SAYFASI METODLARI

    // Login formunun doldurulması
    @Step("<kullaniciAdi> ve <sifre> bölümünü doldur")
    public void loginFormDoldur(String kullaniciAdi, String sifre) {
        if (chromedriver.getTitle().equals(girisTitle)) {
            System.out.println("----LOGIN SAYFASI YUKLENDI----");
            chromedriver.findElement(By.id("txtUserName")).sendKeys(kullaniciAdi);
            chromedriver.findElement(By.id("txtPassword")).sendKeys(sifre);
        }else{
            System.out.println("----LOGIN SAYFA YUKLEMESI BAŞARISIZ----");
        }
    }

    // Grişi Butonuna Tıklama
    @Step("Giriş yap")
    public void girisYap() {
        chromedriver.findElement(By.id("btnLogin")).submit();
    }




    // HESABIM SAYFASI METODLARI

    @Step("<isim> için ad kontrolü")
    public void isimKontrol(String isim) {

        if (chromedriver.getTitle().equals(girisTitle)) {
            System.out.println("----HESABIM SAYFASI YUKLENDI----");
            WebElement isimElement = chromedriver.findElement(By.xpath("//*[@id=\"AccountMenu\"]/div/div/div[1]/div/div[1]/div[2]/strong"));

            if(isimElement.getText().equals(isim)){
                System.out.println("-----İSİM DOĞRU----");
            }
            else {
                System.out.println("-----İSİM YANLIŞ----");
            }
        }else{
            System.out.println("----HESABIM SAYFASI YUKLENEMEDI----");
        }

    }

}
